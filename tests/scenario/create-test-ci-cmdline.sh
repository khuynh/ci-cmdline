#!/bin/bash
set -ex
test_project_name=test-ci-cmdline2
admin_credentials="$SECRETS/thierry_martinez_ci_credentials.yml"
export CI_CREDENTIALS="$admin_credentials"

if ci project list short_name="$test_project_name"; then
    ci project delete "$test_project_name" --really
fi

ci project create "$test_project_name" 'Test project for ci command-line' 'Test project for ci command-line'
