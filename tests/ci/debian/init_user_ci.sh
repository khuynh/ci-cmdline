#!/bin/bash
set -ex
adduser --disabled-password --gecos ci --shell /bin/bash ci
echo ci ALL=\(ALL\) NOPASSWD:ALL >>/etc/sudoers
# Get rid of the "setrlimit(RLIMIT_CORE): Operation not permitted" warning
# See https://bugzilla.redhat.com/show_bug.cgi?id=1773148
echo Set disable_coredump false >>/etc/sudo.conf
