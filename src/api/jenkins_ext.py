"""
Jenkins API

Some API entry points are missing in python-jenkins, so we implement them here.
"""

import json
import six.moves.http_client
import urllib
import time
import typing

import bs4
import jenkins
import requests

import api.crawler

LAUNCH_SLAVE_AGENT = "computer/%(name)s/launchSlaveAgent"

DISCONNECT_NODE = "computer/%(name)s/doDisconnect"

TERM_BUILD = "%(folder_url)sjob/%(short_name)s/%(number)s/term"

KILL_BUILD = "%(folder_url)sjob/%(short_name)s/%(number)s/kill"

REPLAY_BUILD = "%(folder_url)sjob/%(short_name)s/%(number)s/replay/run"

LOG_COMPUTER = "computer/%(name)s/logText/progressiveText"

LOG_JOB = "%(folder_url)sjob/%(short_name)s/%(number)s/logText/progressiveText"

TOKENS = "user/%(username)s/descriptorByName/jenkins.security.ApiTokenProperty"

CREATE_TOKEN = f"{TOKENS}/generateNewToken"

REVOKE_TOKEN = f"{TOKENS}/revoke"

CONFIGURE_USER = "user/%(username)s/configure"

BUILD_HOOK = "%(folder_url)sjob/%(short_name)s/build"

PIPELINE_MODEL_VALIDATE = "pipeline-model-converter/validate"


# There are many methods because the API has many entry points.
# Perhaps should we categorize?
# pylint: disable=too-many-public-methods
class JenkinsExt(jenkins.Jenkins):
    """Jenkins API with some extensions"""

    def __query(self, url: str):
        try:
            return json.loads(self.jenkins_open(requests.Request("GET", url)))
        except (
            requests.exceptions.HTTPError,
            six.moves.http_client.BadStatusLine,
        ):
            raise jenkins.BadHTTPException(
                "Error communicating with server[{self.server}]"
            )
        except ValueError:
            raise jenkins.JenkinsException(
                "Could not parse JSON info for server[{self.server}]"
            )

    def get_nodes_full(self, depth=0):
        """Get a list of nodes connected to the Master"""
        return self.__query(
            self._build_url(jenkins.NODE_LIST, dict(depth=depth))
        )["computer"]

    def get_job_queue(self):
        """Get job queue"""
        return self.__query(self._build_url("queue/api/json"))["items"]

    def cancel_queued_job(self, job_id: int) -> None:
        """Cancel queued job"""
        try:
            self.jenkins_request(
                requests.Request(
                    "POST", self._build_url(f"queue/cancelItem?id={job_id}")
                )
            )
        except jenkins.NotFoundException:
            # cancelItem redirects to /queue/ which does not exist...
            pass

    def launch_slave_agent(self, name: str) -> None:
        """Launch Jenkins agent on node.

        :param name: Name of Jenkins node, ``str``
        """
        self.jenkins_request(
            requests.Request(
                "POST", self._build_url(LAUNCH_SLAVE_AGENT, dict(name=name))
            )
        )

    def disconnect_node(
        self, name: str, offline_message: typing.Optional[str] = None
    ) -> None:
        """Disconnect Jenkins node.

        :param name: Name of Jenkins node, ``str``
        """
        data = {}
        if offline_message is not None:
            data["offlineMessage"] = offline_message
        self.jenkins_request(
            requests.Request(
                "POST",
                self._build_url(DISCONNECT_NODE, dict(name=name)),
                data=data,
            )
        )

    def terminate_build(self, name, number):
        """Terminate a running Jenkins build.

        :param name: Name of Jenkins job, ``str``
        :param number: Jenkins build number for the job, ``int``
        """
        folder_url, short_name = self._get_job_folder(name)
        self.jenkins_request(
            requests.Request(
                "POST",
                self._build_url(
                    TERM_BUILD,
                    dict(
                        folder_url=folder_url,
                        short_name=short_name,
                        number=number,
                    ),
                ),
            )
        )

    def replay_build(self, name, number, jenkinsfile):
        """Replay a Jenkins build with a specified Jenkinsfile.

        :param name: Name of Jenkins job, ``str``
        :param number: Jenkins build number for the job, ``int``
        :param jenkinsfile: Jenkinsfile content, ``str``
        """
        folder_url, short_name = self._get_job_folder(name)
        self.jenkins_request(
            requests.Request(
                "POST",
                self._build_url(
                    REPLAY_BUILD,
                    dict(
                        folder_url=folder_url,
                        short_name=short_name,
                        number=number,
                    ),
                ),
                data={"json": json.dumps({"mainScript": jenkinsfile})},
            )
        )

    def kill_build(self, name, number):
        """Kill a running Jenkins build.

        :param name: Name of Jenkins job, ``str``
        :param number: Jenkins build number for the job, ``int``
        """
        folder_url, short_name = self._get_job_folder(name)
        self.jenkins_request(
            requests.Request(
                "POST",
                self._build_url(
                    KILL_BUILD,
                    dict(
                        folder_url=folder_url,
                        short_name=short_name,
                        number=number,
                    ),
                ),
            )
        )

    def __show_log(self, url: str, follow: bool):
        """show log, keeping connection if necessary"""
        start = 0
        while True:
            response = self.jenkins_request(
                requests.Request("GET", url, params={"start": str(start)})
            )
            print(response.text, end="")
            if not follow:
                break
            try:
                more_data = response.headers["X-More-Data"] == "true"
            except KeyError:
                more_data = False
            if not more_data:
                break
            start += len(response.content)
            time.sleep(1)

    def show_computer_log(self, name: str, follow=False):
        """show computer log, keeping connection if necessary"""
        self.__show_log(self._build_url(LOG_COMPUTER, dict(name=name)), follow)

    def show_build_log(self, name: str, number: int, follow=False):
        """show build log, keeping connection if necessary"""
        folder_url, short_name = self._get_job_folder(name)
        self.__show_log(
            self._build_url(
                LOG_JOB,
                dict(
                    folder_url=folder_url, short_name=short_name, number=number
                ),
            ),
            follow,
        )

    def get_node_config_raw(self, name):
        """Get the configuration for a node.

        :param name: Jenkins node name, ``str``
        """
        get_config_url = self._build_url(jenkins.CONFIG_NODE, dict(name=name))
        return self.jenkins_request(requests.Request("GET", get_config_url))

    def reconfig_node_raw(self, name, config_xml):
        """Change the configuration for an existing node.

        :param name: Jenkins node name, ``str``
        :param config_xml: New XML configuration, ``str``
        """
        reconfig_url = self._build_url(jenkins.CONFIG_NODE, dict(name=name))
        self.jenkins_request(
            requests.Request(
                "POST",
                reconfig_url,
                data=config_xml,
                headers=jenkins.DEFAULT_HEADERS,
            )
        )

    def create_token(
        self, username: str, name: typing.Optional[str] = None
    ) -> str:
        """create a new token"""
        data = {}
        if name is not None:
            data["newTokenName"] = name
        url = self._build_url(CREATE_TOKEN, dict(username=username))
        response = self.jenkins_open(requests.Request("POST", url, data=data))
        return json.loads(response)["data"]["tokenValue"]

    def revoke_token(self, username: str, uuid: str) -> None:
        """create a new token"""
        data = {"tokenUuid": uuid}
        url = self._build_url(REVOKE_TOKEN, dict(username=username))
        self.jenkins_request(requests.Request("POST", url, data=data))

    def get_tokens(self, username: str):
        """get user tokens"""
        url = self._build_url(CONFIGURE_USER, dict(username=username))
        response = self.jenkins_request(requests.Request("GET", url))
        page = bs4.BeautifulSoup(response.content, features="lxml")
        if page.body is None:
            raise Exception("Expected body")
        return [
            {
                "_uuid": item.find("input", {"name": "tokenUuid"})["value"],
                "name": item.find("input", {"name": "tokenName"})["value"],
                "creation_date": item.find("span", class_="token-creation")[
                    "title"
                ],
                "use": item.find(
                    "span", class_="token-use-counter"
                ).text.strip(),
            }
            for item in page.body.find_all(
                "div", class_="token-list-existing-item"
            )
        ]

    def pipeline_model_validate(self, jenkinsfile: str):
        """validate pipeline model"""
        url = self._build_url(PIPELINE_MODEL_VALIDATE)
        return self.jenkins_open(
            requests.Request("POST", url, data=dict(jenkinsfile=jenkinsfile))
        )

    def close(self) -> None:
        """close connection"""
        self._session.close()

    def _build_url_auth(
        self, format_spec, username: str, password: str, variables=None
    ):
        if variables:
            url_path = format_spec % self._get_encoded_params(variables)
        else:
            url_path = format_spec
        assert self.server.startswith("https://")
        server_name = self.server[len("https://") :]
        prefix = f"\
https://{urllib.parse.quote(username)}:{urllib.parse.quote(password)}@\
{server_name}"
        return str(urllib.parse.urljoin(prefix, url_path))

    def get_build_hook_url(
        self, job_name: str, username: str, password: str
    ) -> str:
        """return build hook URL"""
        folder_url, short_name = self._get_job_folder(job_name)
        return self._build_url_auth(
            BUILD_HOOK,
            username,
            password,
            dict(folder_url=folder_url, short_name=short_name),
        )

    def check_available_upgrade(self) -> typing.Optional[str]:
        """return the version available for upgrade if any"""
        url = self._build_url("manage")
        response = self.jenkins_request(requests.Request("GET", url))
        page = bs4.BeautifulSoup(response.content, features="lxml")
        alert = page.find("div", {"class": "alert alert-info"})
        if not isinstance(alert, bs4.element.Tag):
            return None
        strong = api.crawler.get_child_tag(alert, "strong")
        return strong.text

    def upgrade(self) -> None:
        """upgrade Jenkins to the last available version"""
        url = self._build_url("updateCenter/upgrade")
        self.jenkins_request(requests.Request("POST", url))

    def safe_restart(self) -> None:
        """
        restart Jenkins when installation is complete and no jobs are running
        """
        url = self._build_url("updateCenter/safeRestart")
        try:
            self.jenkins_request(requests.Request("POST", url))
        except requests.exceptions.HTTPError:
            pass

    def install_plugin(self, name, include_dependencies=True):
        """
        (Implementation in jenkins package is bugged. Monkey patch here!)
        Install a plugin and its dependencies from the Jenkins public
        repository at http://repo.jenkins-ci.org/repo/org/jenkins-ci/plugins

        :param name: The plugin short name, ``string``
        :param include_dependencies: Install the plugin's dependencies, ``bool``
        :returns: Whether a Jenkins restart is required, ``bool``

        Example::
            >>> info = server.install_plugin("jabber")
            >>> print(info)
            True
        """
        # using a groovy script because Jenkins does not provide a REST endpoint
        # for installing plugins.
        install = (
            'Jenkins.instance.updateCenter.getPlugin("' + name + '")'
            ".deploy();"
        )
        if include_dependencies:
            install = (
                'Jenkins.instance.updateCenter.getPlugin("' + name + '")'
                ".getNeededDependencies().each{it.deploy()};"
            ) + install

        self.run_script(install)
        # run_script is an async call to run groovy. we need to wait a little
        # before we can get a reliable response on whether a restart is needed
        time.sleep(2)
        is_restart_required = (
            "print(Jenkins.instance.updateCenter"
            '.isRestartRequiredForCompletion());print("\\n");'
        )

        # response is a string (i.e. u'Result: true\n'), return a bool instead
        response_str = self.run_script(is_restart_required)
        response = response_str == "true"
        return response
