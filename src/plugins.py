"""Managing Jenkins plugins"""

import typing

import api.spec
import commandline
import connections
import jenkins_instances
import projects
import tables
import volumes

LIST_SPEC = {
    "short_name": "shortName",
    "long_name": "longName",
    "has_update": "hasUpdate",
}

PluginListFilter = typing.NewType("PluginListFilter", str)
tables.init_list_filter_type(PluginListFilter, LIST_SPEC)

Name = typing.NewType("Name", str)
Name.__doc__ = "plugin name"

Version = typing.NewType("Version", str)
Version.__doc__ = "plugin version (default: latest)"


@commandline.RegisterCategory
class Plugin(commandline.Category):
    """managing Jenkins plugins"""

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[PluginListFilter],
    ) -> None:
        """list Jenkins plugins"""
        volumes.simple_list(
            self.config,
            project,
            list_filter,
            lambda connection: api.spec.remap_dict_list(
                connection.jenkins.get_plugins_info(), LIST_SPEC
            ),
        )

    @commandline.RegisterAction
    def install(
        self,
        project: projects.Name,
        plugin: Name,
        no_restart=jenkins_instances.NoRestart(False),
    ) -> None:
        """install Jenkins plugin"""
        with connections.Connections(
            self.config, subdomain=project
        ) as connection:
            restart_needed = connection.jenkins.install_plugin(plugin)
            if restart_needed and not no_restart:
                connection.jenkins.safe_restart()
