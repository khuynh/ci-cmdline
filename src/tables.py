"""Tables generation and formatting"""

# PEP 563 -- Postponed Evaluation of Annotations
# https://www.python.org/dev/peps/pep-0563/
# For instance, Condition.parse returns a Condition.
from __future__ import annotations
import collections
import re
import typing

import api.crawler
import commandline


def init_filter_type(filter_type: type, *specs: api.spec.DictSpec) -> None:
    """make a new filter according to table specifications"""
    columns = sorted(
        set.union(
            *(
                set(
                    spec.keys()
                    if isinstance(spec, collections.abc.Mapping)
                    else spec
                )
                for spec in specs
            )
        )
    )
    filter_type.__doc__ = f"""\
        COLUMN[!][~]=VALUE

        Filters can be of the form COLUMN=VALUE or COLUMN~=REGEX.
        Filters can be negated with != and
        !~= (remember to quote the argument when calling from a shell).
        Possible values for COLUMN are: {', '.join(columns)}"""

    def completer(**_kwargs):
        return columns

    commandline.set_completer(filter_type, completer)


def init_list_filter_type(
    list_filter_type: type, *specs: api.spec.DictSpec
) -> None:
    """make a new list filter according to table specifications"""
    columns = sorted(
        set.union(
            *(
                set(
                    spec.keys()
                    if isinstance(spec, collections.abc.Mapping)
                    else spec
                )
                for spec in specs
            )
        )
    )
    list_filter_type.__doc__ = f"""\
        COLUMN or COLUMN[!][~]=VALUE

        If filters of the form COLUMN=VALUE or COLUMN~=REGEX are provided,
        only matching results are displayed. Filters can be negated with != and
        !~= (remember to quote the argument when calling from a shell).
        If column names of the form COLUMN are provided, only matching columns
        are displayed.
        Possible values for COLUMN are: {', '.join(columns)}"""

    def completer(**_kwargs):
        return columns

    commandline.set_completer(list_filter_type, completer)


def show_table(table, compact):
    """prints a comma-separated 2D table"""
    if not table:
        return
    column_width = [
        max([len(row[i]) for row in table]) for i, _ in enumerate(table[0])
    ]
    for row in table:
        for i, cell in enumerate(row):
            if i + 1 == len(row):
                print(cell)
            elif compact:
                print(cell + ", ", end="")
            else:
                print((cell + ",").ljust(column_width[i] + 2), end="")


Row = typing.Mapping[str, typing.Any]
Table = typing.List[Row]


def show_dict(dict_table: Table, compact: bool):
    """prints a list of dictionaries as a comma-separated 2D table"""
    if not dict_table:
        return
    rows = []
    head = dict_table[0].keys()
    if len(head) > 1:
        rows.append(list(head))
    for fields in dict_table:
        rows.append(list([str(fields.get(key, "")) for key in head]))
    show_table(rows, compact)


ConditionValue = typing.Union[str, typing.Pattern[str]]


class Condition:
    """row condition"""

    def __init__(self, key: str, value: ConditionValue, negated: bool):
        self.__key = key
        self.__value = value
        self.__negated = negated

    @property
    def key(self) -> str:
        """return the condition key"""
        return self.__key

    @property
    def value(self) -> ConditionValue:
        """return the condition value"""
        return self.__value

    @property
    def negated(self) -> bool:
        """return whether condition is negated"""
        return self.__negated

    def get_simple_equality(self) -> typing.Optional[str]:
        """return s if the condition is a simple equality check to s,
        None otherwise"""
        value = self.__value
        if self.__negated or not isinstance(value, str):
            return None
        return value

    def check(self, row: Row) -> bool:
        """check that a table row satisfies the condition"""
        value = self.__value
        cell = str(row[self.__key])
        if isinstance(value, str):
            test = cell == value
        else:
            test = value.search(cell) is not None
        return test != self.__negated

    @classmethod
    def parse(cls, condition: str) -> typing.Optional[Condition]:
        """parse a condition, returning None if the string is not a condition
        (i.e., does not contain any '=')"""
        values = condition.split("=", 1)
        try:
            key, value_str = values
        except ValueError:
            return None
        if key[-2:] == "!~":
            key = key[:-2]
            negated = True
            value: ConditionValue = re.compile(value_str)
        elif key[-1:] == "~":
            key = key[:-1]
            negated = False
            value = re.compile(value_str)
        elif key[-1:] == "!":
            key = key[:-1]
            negated = True
            value = value_str
        else:
            negated = False
            value = value_str
        return Condition(key, value, negated)


class Filters(typing.NamedTuple):
    """pair of columns and conditions for table filtering"""

    columns: typing.Iterable[str]
    conditions: typing.Iterable[Condition]

    @classmethod
    def parse(cls, filters: typing.Iterable[str]) -> Filters:
        """parse table filters"""
        columns = []
        conditions = []
        for condition_str in filters:
            condition = Condition.parse(condition_str)
            if condition:
                conditions.append(condition)
            else:
                columns.append(condition_str)
        return Filters(columns, conditions)

    def check_row(self, row: Row) -> bool:
        """checks if a row is satisfied"""
        return all(condition.check(row) for condition in self.conditions)

    def add_row(self, table: Table, row: Row) -> None:
        """adds in table the row fields with first column id.
        fields is a dictionary, which associates columns to values,
        and table is a list of such dictionaries.
        columns is a list of column titles: if columns is empty,
        all columns are added, otherwise only columns
        listed in columns are added.
        The row is added only if all predicates in conditions are satisfied."""
        conditions = self.conditions
        columns = self.columns
        entry = {}
        try:
            if not self.check_row(row):
                return
            for condition in conditions:
                if not condition.check(row):
                    return
            if columns:
                entry = {key: row[key] for key in columns}
            else:
                entry = {
                    key: value for key, value in row.items() if key[0] != "_"
                }
        except KeyError as error:
            raise commandline.Failure(
                f"""
                Unknown column {error}: columns are {", ".join(row.keys())}."""
            )
        table.append(entry)

    def show_list(self, rows: typing.Iterable[Row], compact: bool) -> None:
        """implements 'list' commands: shows the table stored in dict_to_list
        according to filters, which contain either column names or
        conditions."""
        table: Table = []
        for row in rows:
            self.add_row(table, row)
        if table == []:
            raise commandline.Failure("No match.")
        show_dict(table, compact)

    def filter_list(self, rows: typing.Iterable[Row]) -> typing.List[Row]:
        """filter list"""
        return [row for row in rows if self.check_row(row)]


def rows_of_dict(
    dictionary: typing.Mapping[typing.Any, Row]
) -> typing.Iterable[Row]:
    """convert a dictionary to a table"""
    return [
        dict([("id", str(key))] + list(columns.items()))
        for key, columns in dictionary.items()
    ]


def dict_of_list(
    items: typing.Iterable[typing.Mapping[str, typing.Any]], key: str
) -> typing.Mapping[typing.Any, typing.Mapping[str, typing.Any]]:
    """transform a list of dictionaries into a dictionary of dictionaries"""
    return {
        item[key]: dict(filter(lambda entry: entry[0] != key, item.items()))
        for item in items
    }


def filter_and_show_list(
    rows: typing.Iterable[Row], filters: typing.Iterable[str], compact: bool
) -> None:
    """implements 'list' commands: shows the table stored in dict_to_list
    according to filters, which contain either column names or conditions."""
    Filters.parse(filters).show_list(rows, compact)


def filter_and_show_listable(
    rows: typing.Iterable[api.spec.Listable],
    filters: typing.Iterable[str],
    compact: bool,
) -> None:
    """implements 'list' commands for listable elements"""
    filter_and_show_list(listable_to_table(rows), filters, compact)


def filter_list(
    rows: typing.Iterable[Row], filters: typing.Iterable[str]
) -> typing.List[Row]:
    """filter list"""
    return Filters.parse(filters).filter_list(rows)


def listable_to_table(
    rows: typing.Iterable[api.spec.Listable],
) -> typing.Sequence[Row]:
    """convert a list of Listable to a list of rows"""
    return [row.as_map for row in rows]


# T seems legit for a type variable!
# pylint: disable=invalid-name
T = typing.TypeVar("T")


def match_dict(
    dict_to_match: typing.Dict[str, T],
    query: str,
    should_match: typing.Optional[str] = None,
    unambiguous: typing.Optional[str] = None,
) -> typing.Iterable[T]:
    """returns the entries of the dictionary dict_to_match that match query.
    If query appears as is as key in dict_to_match, the associated value
    is returned. Otherwise, the function returns the list of values
    such as key matches query interpreted as regular expression."""
    try:
        return [dict_to_match[query]]
    except KeyError:
        compiled_query = re.compile(query, flags=re.IGNORECASE)
        entries = [
            (key, value)
            for key, value in dict_to_match.items()
            if compiled_query.search(key)
        ]
        if not entries and should_match is not None:
            raise commandline.Failure(
                f"{query} does not match any {should_match}"
            )
        if len(entries) > 1 and unambiguous is not None:
            matches = ", ".join([key for key, _value in entries])
            raise commandline.Failure(
                f"{query} is ambiguous for {unambiguous}: {matches}"
            )
        return [value for _key, value in entries]


def find_offer(offers: typing.Dict[str, str], offer: str, kind: str) -> str:
    """find offer in offers, where offers maps offer ids to offer names.
    offer can be either an id or an offer name or a (non ambiguous) regular
    expression on offer names. kind is used for the error message."""
    if offer in offers:
        return offer
    dict_to_match = {name: offer_id for offer_id, name in offers.items()}
    (offer_id,) = match_dict(
        dict_to_match, offer, should_match=kind, unambiguous=kind
    )
    return offer_id
