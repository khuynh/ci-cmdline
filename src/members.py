"""Managing members"""

import time
import typing

import api.crawler
import api.portal
import commandline
import connections
import tables
import projects


def complete_member(**kwargs):
    """complete members in command-line"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials
    ) as connection:
        return match_table(
            connection.portal.projects[parsed_args.project]
        ).keys()


Name = typing.NewType("Name", str)
Name.__doc__ = "project member (login or email)"
commandline.set_completer(Name, complete_member)


def complete_pending_name(**kwargs):
    """complete pending users in command-line"""
    parsed_args = kwargs["parsed_args"]
    with connections.NoninteractiveConnections(
        parsed_args.credentials
    ) as connection:
        return match_table_pending(
            connection.portal.projects[parsed_args.project]
        ).keys()


PendingName = typing.NewType("PendingName", str)
PendingName.__doc__ = """
    pending user (name or email)

    PENDING are regular expressions which can match either pending user
    names or emails. Multiple matches are accepted, but the command will
    fail if a regular expression has no match.
    """
commandline.set_completer(PendingName, complete_pending_name)


MemberListFilter = typing.NewType("MemberListFilter", str)
tables.init_list_filter_type(
    MemberListFilter,
    api.crawler.MEMBER_LIST_SPEC_MEMBER,
    api.crawler.MEMBER_LIST_SPEC_NOT_MEMBER,
    api.crawler.PENDING_USER_LIST_SPEC,
)


@commandline.RegisterCategory
class Member(commandline.Category):
    """managing project members"""

    Email = typing.NewType("Email", str)
    Email.__doc__ = "user email"

    Admin = typing.NewType("Admin", bool)
    Admin.__doc__ = "project administrator"

    VmAdmin = typing.NewType("VmAdmin", bool)
    VmAdmin.__doc__ = "virtual machine administrator"

    @commandline.RegisterAction
    def add(
        self,
        project: projects.Name,
        email: typing.Iterable[Email],
        admin: typing.Optional[Admin] = Admin(False),
        vm_admin: typing.Optional[VmAdmin] = VmAdmin(False),
    ) -> None:
        """add members on project"""
        with connections.Connections(self.config) as connection:
            members = connection.portal.projects[project].members
            members.add(*(connection.portal.users[user] for user in email))
            if admin or vm_admin:
                while True:
                    member_dict = {
                        member_obj.email: member_obj for member_obj in members
                    }
                    try:
                        member_objs = (member_dict[user] for user in email)
                        break
                    except KeyError:
                        time.sleep(1)
                        continue
                for member_obj in member_objs:
                    if admin:
                        member_obj.set(api.portal.Member.Role.ADMIN, True)
                    if vm_admin:
                        member_obj.set(api.portal.Member.Role.SLAVE_ADMIN, True)

    @commandline.RegisterAction
    def delete(
        self, project: projects.Name, member: typing.Iterable[Name]
    ) -> None:
        """remove members from project"""
        invalid = []
        with connections.Connections(self.config) as connection:
            for member_obj in find(connection.portal.projects[project], member):
                try:
                    member_obj.leave()
                except api.connection.QueryError as error:
                    invalid.append(f"{member_obj.email} ({error})")
        commandline.report_invalid(invalid)

    Pending = typing.NewType("Pending", bool)
    Pending.__doc__ = "pending users"

    @commandline.RegisterAction
    def list(
        self,
        project: projects.Name,
        list_filter: typing.Iterable[MemberListFilter],
        pending: Pending = Pending(False),
    ) -> None:
        """list project members"""
        with connections.Connections(self.config) as connection:
            if pending:
                rows = connection.portal.projects[project].members.pendings
            else:
                rows = connection.portal.projects[project].members
        tables.filter_and_show_listable(rows, list_filter, self.config.compact)

    @commandline.RegisterAction
    def set(
        self,
        project: projects.Name,
        member: typing.Iterable[Name],
        admin: typing.Optional[Admin] = None,
        vm_admin: typing.Optional[VmAdmin] = None,
    ) -> None:
        """set admins or virtual machine admins on project"""
        invalid = []
        with connections.Connections(self.config) as connection:
            for member_obj in find(connection.portal.projects[project], member):
                try:
                    if admin is not None:
                        member_obj.set(api.portal.Member.Role.ADMIN, admin)
                    if vm_admin is not None:
                        member_obj.set(
                            api.portal.Member.Role.SLAVE_ADMIN, vm_admin
                        )
                except api.connection.QueryError as error:
                    invalid.append(f"{member_obj.email} ({error})")
        commandline.report_invalid(invalid)

    @commandline.RegisterAction
    def accept(
        self, project: projects.Name, pending: typing.Iterable[PendingName]
    ) -> None:
        """accept pending users"""
        with connections.Connections(self.config) as connection:
            accept = find_pending(connection.portal.projects[project], pending)
            connection.portal.projects[project].members.pendings.answer(
                accept=accept
            )

    @commandline.RegisterAction
    def reject(
        self, project: projects.Name, pending: typing.Iterable[PendingName]
    ) -> None:
        """reject pending users"""
        with connections.Connections(self.config) as connection:
            reject = find_pending(connection.portal.projects[project], pending)
            connection.portal.projects[project].members.pendings.answer(
                reject=reject
            )


def match_table(project: api.portal.Project):
    """return a dictionary of members indexed by uid and by
    email."""
    return {
        key: member
        for member in project.members
        for key in (member.uid, member.email)
    }


def find(
    project: api.portal.Project, patterns: typing.Iterable[Name]
) -> typing.Iterable[api.portal.Member]:
    """find a member by uid or by email."""
    members = match_table(project)
    return {
        member
        for pattern in patterns
        for member in tables.match_dict(members, pattern, should_match="member")
    }


def match_table_pending(project: api.portal.Project):
    """return a dictionary of pending users indexed by name and by
    e-mail."""
    return {
        key: pending
        for pending in project.members.pendings
        for key in (pending.full_name, pending.email)
    }


def find_pending(
    project: api.portal.Project, patterns: typing.Iterable[PendingName]
) -> typing.Iterable[api.portal.PendingUser]:
    """find a pending user by name or by email."""
    pendings = match_table_pending(project)
    return {
        user
        for pattern in patterns
        for user in tables.match_dict(
            pendings, pattern, should_match="pending user"
        )
    }
